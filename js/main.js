// button open menu

$('.nav-header-body .open-nav').click(function() {
    $('.nav-header-body').toggleClass('active');
 });

 //close popup
 $('.button-close').click(function() {
  $('.popup-login').removeClass('active');
});

 //close popup
 $('#open-modal-account').click(function() {
  $('.popup-login').addClass('active');
});

$('#open-modal-signup').click(function() {
  $('.popup-signup').addClass('active');
  $('.popup-login').removeClass('active');
});


//slider three next prev

$(document).on('ready', function() {
    $(".variable").slick({
      dots: false,
      infinite: true,
      slidesToShow: 2,
      variableWidth: true
    });
  });


  // cussor
  const initSlider = (selector, options = {}) => {
    const sliderContainers = document.querySelectorAll(selector)
  
    if (sliderContainers.length) {
      sliderContainers.forEach(container => {
        const slider = container.querySelector('.swiper-container')
        const paging = container.querySelector('.js-paging')
        let prev;
        let next;
  
        if (paging) {
          prev = paging.querySelector('.js-paging-prev')
          next = paging.querySelector('.js-paging-next')
        }
  
        const swiper = new Swiper (slider, {
          loop: false,
          speed: 1000,
          slidesPerView: 1,
          spaceBetween: 10,
          grabCursor: true,
          breakpoints: {
            600: {
              slidesPerView: 2,
              spaceBetween: 18,
              freeMode: true
            }
          },
          ...options
        })
  
        next.addEventListener('click', () => {
          swiper.slideNext(1000);
        })
        prev.addEventListener('click', () => {
          swiper.slidePrev(1000);
        })
  
        // Used for animations on slider dragging
        swiper.on('touchMove', ({el}) => {
          el.classList.add('dragged')
        })
        swiper.on('touchEnd', ({el}) => {
          el.classList.remove('dragged')
        })
      })
    }
  }
  
  initSlider('.js-slider')
  
  
  //---------------------
  // CURSOR CODE
  //---------------------
  
  
  // set the starting position of the cursor outside of the screen
  let clientX = -100;
  let clientY = -100;
  let lastX = -100;
  let lastY = -100;
  const cursor = document.querySelector('.cursor')
  const section = document.querySelector('.slider')
  
  // Show/hide the cursor when it is over the section
  if (section) {
    section.addEventListener('mouseenter', () => {
      cursor.classList.add('visible')
    })
  
    section.addEventListener('mouseleave', () => {
      cursor.classList.remove('visible')
    })
  }
  
  // function for linear interpolation of values
  const lerp = (a, b, n) => {
    return (1 - n) * a + n * b;
  };
  
  const initCursor = () => {
    if (!cursor) return
  
    // add listener to track the current mouse position
  document.addEventListener('mousemove', e => {
      clientX = e.clientX;
      clientY = e.clientY;
    });
  
    // transform the cursor to the current mouse position
    // use requestAnimationFrame() for smooth performance
    const render = () => {
      // lesser delta, greater the delay that the custom cursor follows the real cursor
      const delta = 0.1;
      lastX = lerp(lastX, clientX, delta);
      lastY = lerp(lastY, clientY, delta);
  
      cursor.style.transform = `translate(${lastX}px, ${lastY}px)`;
  
      requestAnimationFrame(render);
    };
  
    requestAnimationFrame(render);
  };
  
  initCursor();
  


  //rating
  document.querySelectorAll('#star').forEach(star => {
    star.addEventListener('mouseover', animateStart);
    star.addEventListener('mouseout', animateEnd);
    star.addEventListener('click', set) 
  })
  
  function animateStart(e) { 
   const index = e.target.getAttribute('data-index')
    for(let i = 0; i <= index; i++){ e.target.parentNode.children[i].classList.add('animated')
    }
   
  }
  function animateEnd(e) { 
    const index = e.target.getAttribute('data-index')
    for(let i = 0; i <= index; i++){ e.target.parentNode.children[i].classList.remove('animated')
    }
  }
  function set(e) {
     const index = e.target.getAttribute('data-index');
    for(let i = 0; i <= 4; i++){ 
       e.target.parentNode.children[i].classList.remove('active')
    }
    for(let i = 0; i <= index; i++){ e.target.parentNode.children[i].classList.add('active')
    }
  }